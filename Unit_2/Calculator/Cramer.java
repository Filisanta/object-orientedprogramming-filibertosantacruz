using System;

namespace SalveM
{
    class Cramer
    {
        public void cramm()
        {
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║   La Matriz a resolver                          ║ ");
            Console.WriteLine("\t\t║   Ingresa el Valor X11                          ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
            Console.WriteLine("\t\t                                                    ");
            Console.WriteLine("\t\t                                                    ");
                Console.ForegroundColor = ConsoleColor.Blue;

            Console.WriteLine("X11 Y11 Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double X11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y11");

            Console.WriteLine(X11 + " Y11 Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Y11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z11");

            Console.WriteLine(X11 + " " + Y11 + " Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Z11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor T1");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double T1 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine("Ingresa el Valor X22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double X22 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Y22 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Z22 = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Ingresa el Valor T2");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double T2 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine("Ingresa el Valor X33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine("X33 Y33 Z33 T3");

            double X33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " Y33 Z33 T3");

            double Y33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " Z33 T3");
            double Z33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor T3");
            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " " + Z33 + " T3");
            double T3 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Tu Matriz final es: ");
            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " " + Z33 + " " + T3);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            //Mostramos La matriz haciendo la primera operacion
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║        Primero obtenemos Delta                  ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
            Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " " + Y11);
            Console.WriteLine();
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.WriteLine();
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine();
            //Esta es la operacion
            double Del = 0;
            Del = (X11 * Y22 * Z33);
            Del = Del + (Y11 * Z22 * X33);
            Del = Del + (Z11 * X22 * Y33);
            Console.WriteLine("");
            //Aqui se muestra la operacion
             Console.Write("(" + X11 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + X33 + ")");
            Console.Write(" + "); Console.Write("(" + Z11 + " * " + X22 + " * " + Y33 + ")");
            //Mostramos La matriz haciendo la segunda operacion operacion
            Console.WriteLine("");
            Console.WriteLine("");
            Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " ");
            Console.Write(Y11);
            Console.WriteLine("");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22);
            Console.WriteLine("");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine("");
            //esta es la segunda operacion
            double ta = 0;
            ta = (Y11 * X22 * Z33);
            ta = ta + (X11 * Z22 * Y33);
            ta = ta + (Z11 * Y22 * X33);
            Double Delta = Del - ta;
            Console.WriteLine("");
            //mostramos la segunda operacion
             Console.Write("(" + Z11 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + X11 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + X22 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("");
            //se muestra la operacion final
             Console.Write("(" + X11 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Z11 + " * " + X22 + " * " + Y33 + ")");
            Console.Write(" - ");
            Console.Write("(" + Z11 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + "); Console.Write("(" + X11 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + ");Console.Write("(" + Y11 + " * " + X22 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Tu DELTA del sistema es:" + Delta);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║              Calcular X                         ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
            //mostramos la matriz para sacar X
            //sera la primera operacion
            Console.Write(T1 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(T1 + " " + Y11);
            Console.WriteLine();
            Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.WriteLine();
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine();
            //aqui se hace la primera operacion
            double E = 0;
            E = (T1 * Y22 * Z33);
            E = E + (Y11 * Z22 * T3);
            E = E + (Z11 * T2 * Y33);
            Console.WriteLine("");
            //aqui se muestra la operacion que se hizo
             Console.Write("(" + T1 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + T3 + ")");
            Console.Write(" + ");
            Console.Write("(" + Z11 + " * " + T2 + " * " + Y33 + ")");
            Console.WriteLine("");
            Console.WriteLine("");
            //mostramos la matriz de la segunda operacion para sacar X
            Console.Write(T1 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(T1 + " ");
            Console.Write(Y11);
            Console.WriteLine("");
            Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Y22);
            Console.WriteLine("");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine("");
            //esta fue la segunda operacion para sacar X
            double quis = 0;
            quis = (Y11 * X22 * Z33);
            quis = quis + (X11 * Z22 * Y33);
            quis = quis + (Z11 * Y22 * X33);
            Double xxx = E - quis;
            xxx = xxx / Delta;
            Console.WriteLine("");
            //Aqui mostramos la operacion que se hizo
             Console.Write("(" + Z11 + " * " + Y22 + " * " + T3 + ")");
            Console.Write(" + ");
            Console.Write("(" + T1 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + T2 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("");
            //Aqui mostramos la operacion final
             Console.Write("(" + T1 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + T3 + ")");
            Console.Write(" + ");
            Console.Write("(" + Z11 + " * " + T2 + " * " + Y33 + ")");
            Console.Write(" - ");
            Console.Write("(" + Z11 + " * " + Y22 + " * " + T3 + ")");
            Console.Write(" + ");Console.Write("(" + T1 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + "); Console.Write("(" + Y11 + " * " + T2 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("                                " + Delta);
            Console.WriteLine("");
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            Console.WriteLine(" X es " + xxx);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
        }
    }
}

