using System;
using System.Collections.Generic;
using System.Text;

namespace SalveM 
{
    class  Class1
    {
        public void Cargar()
        {
            Class1 matriss = new Class1();
            String eleccion_menu= null;
            do
            {
                Console.WriteLine("\t\t  Has elegido Matrices Basicas\n");
                Console.WriteLine ("\t\t  1.Para suma de Matrices\n" );
                Console.WriteLine ("\t\t  2.por resta de Matrices\n" );
                Console.WriteLine ("\t\t  3.Para Multiplicacione de Matrices\n");
                Console.WriteLine("\t\t  4. Para salir");
                Console.ForegroundColor = ConsoleColor.Yellow;
                eleccion_menu = Console.ReadLine();
                switch (eleccion_menu)
                {
                    case "1":                        Console.Clear();
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║                  SUMA DE MATRICES               ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.WriteLine("\n");
                        matriss .matrissuma(eleccion_menu);
                        Console.WriteLine("\t\tMENU PRINCIPAL");
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║            Ha Elegido una resta de Matrices     ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.WriteLine("\n");
                        matriss .matrissuma(eleccion_menu);
                        Console.WriteLine("\t\tSeras devuelto al Menu Principal\t\t");
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.ReadLine();
                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║Ha Elegido una multiplicacion de Matrices        ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.WriteLine("\n");
                        matriss.MultiplicacionM();
                        Console.WriteLine("\t\tSeras devuelto al Menu Principal\t\t");
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                        Console.ReadLine();
                        break;
                    case "4":
                        Console.Clear();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Has elegido una opcion Incorrecta");
                        break;
                }
            }
            while (eleccion_menu != "4");
        }

        void matrissuma(string p)
        {

            //numero de FILAS m1
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║    Cuantas filas quieres para tu primera matriz ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            int f11 = int.Parse(Console.ReadLine());
            //numero de COLUMNAS m1
            Console.Clear();
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║   Cuantas columnas quieres para tu primera matriz   ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════════╝");
            int c11 = int.Parse(Console.ReadLine());
            Console.Clear();
            // tamaño de m1
            double[,] m1 = new double[f11, c11];
            //Ingresando valores a m1
            for (int i = 0; i < f11; i++)
            {
                for (int j = 0; j < c11; j++)
                {
                    Console.WriteLine("ingresa el numero [" + i + "] [" + j + "]");
                    m1[i, j] = double.Parse(Console.ReadLine());
                    Console.Clear();
                }
            }
            int f22 = 0;
            int c22 = 0;
            do
            {
                Console.Clear();
                //numero de FILAS m2
                Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                Console.WriteLine("\t\t║    Cuantas filas quieres para tu segunda matriz ║");
                Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                f22 = int.Parse(Console.ReadLine());
                Console.Clear();
                //numero de COLUMNAS m1
                Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                Console.WriteLine("\t\t║    Cuantas filas quieres para tu segunda matriz ║");
                Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                c22 = int.Parse(Console.ReadLine());
                Console.Clear();
                //tamaño de m2
                double[,] m2 = new double[f22, c22];
                //comparacion de Matrices
                if (f11 == f22 && c11 == c22)
                {
                    //creando m3
                    double[,] m3 = new double[f11, c11];
                    //ingresando valores a m2
                    for (int i = 0; i < f22; i++)
                    {
                        for (int j = 0; j < c22; j++)
                        {
                            Console.WriteLine("ingrese el numero [" + i + "] [" + j + "]");
                            m2[i, j] = double.Parse(Console.ReadLine());
                            Console.Clear();
                        }
                    }
                    Console.Clear();
                    //Comparacion
                    //a = suma
                    if (p == "1")
                    {
                        // suma
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                m3[i, j] = m1[i, j] + m2[i, j];
                            }
                        }
                        //imprime m1
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m1[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        //signo de operacion
                        Console.WriteLine("+");
                        //imprime m2
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m2[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        //Total
                        Console.WriteLine("=");
                        //imprime el resultado en m3
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m3[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        Console.WriteLine("\t\tTu suma se a resuelto correctamente\t\t");
                        Console.WriteLine("\n");
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                    }
                    //b = resta
                    if (p == "2")
                    {
                        // hace resta
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                m3[i, j] = m1[i, j] - m2[i, j];
                            }
                        }
                        //imprime m1
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m1[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        //signo de operacion
                        Console.WriteLine("-");
                        //imprime m2
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m2[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        //total
                        Console.WriteLine("=");
                        //imprime el total en m3
                        for (int i = 0; i < f22; i++)
                        {
                            for (int j = 0; j < c22; j++)
                            {
                                Console.Write(m3[i, j] + " ");
                            }
                            Console.WriteLine("");
                        }
                        Console.WriteLine("\t\tTu resta se a resuelto correctamente\t\t");
                        Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                        Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                        Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                    }
                }
                if (f11 != f22 | c11 != c22)
                {
                    Console.WriteLine("\t\tVuelva a intentarlo\t\t");
                    Console.WriteLine("NOTA: Recuerda que tus matrices tienen que tener la misma longitud");
                    Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                    Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                    Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
                    Console.ReadLine();
                }
            }
            while (f11 != f22 | c11 != c22);
            Console.ReadLine();
        }
        void MultiplicacionM()
        {
            //numero de FILAS m1
            Console.WriteLine("\t\tCuantas filas quieres para tu primera matriz");
            int f11 = int.Parse(Console.ReadLine());
            Console.Clear();
            //numero de COLUMNAS m1
            Console.WriteLine("\t\tCuantas columnas quieres para tu primera matriz");
            int c11 = int.Parse(Console.ReadLine());
            Console.Clear();
            // tamaño de m1
            double[,] m1 = new double[f11, c11];
            //Ingresando valores a m1
            for (int i = 0; i < f11; i++)
            {
                for (int j = 0; j < c11; j++)
                {
                    Console.WriteLine("ingresa el numero [" + i + "] [" + j + "]");
                    m1[i, j] = double.Parse(Console.ReadLine());
                    Console.Clear();
                }
            }
            Console.Clear();
            //numero de FILAS m2
            Console.WriteLine("\t\tCuantas filas quieres para tu segunda matriz");
            int f22 = int.Parse(Console.ReadLine());
            Console.Clear();
            //numero de COLUMNAS m1
            Console.WriteLine("\t\tCuantas columnas quieres para tu segunda matriz");
            int c22 = int.Parse(Console.ReadLine());
            Console.Clear();
            //tamaño de m2
            double[,] m2 = new double[f22, c22];
            //comparacion de Matrices
            if (f11 == f22 && c11 == c22 | f11 != c22 && c11 == f22)
            {
                //creando m3
                double[,] m3 = new double[c11, c22];
                //ingresando valores a m2
                for (int i = 0; i < f22; i++)
                {
                    for (int j = 0; j < c22; j++)
                    {
                        Console.WriteLine("ingresa el numero [" + i + "] [" + j + "]");
                        m2[i, j] = double.Parse(Console.ReadLine());
                        Console.Clear();
                    }
                }
                //haciendo la multiplicacion
                for (int i = 0; i < f11; i++)
                {
                    for (int j = 0; j < c22; j++)
                    {
                        for (int k = 0; k < f22; k++)
                        {
                            m3[i, j] += m1[i, k] * m2[k, j];
                        }
                    }
                }
                Console.Clear();
                //imprime m1
                for (int i = 0; i < f11; i++)
                {
                    for (int j = 0; j < c11; j++)
                    {
                        Console.Write(m1[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                //signo de operacion
                Console.WriteLine("*");
                //imprime m2
                for (int i = 0; i < f22; i++)
                {
                    for (int j = 0; j < c22; j++)
                    {
                        Console.Write(m2[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                //total
                Console.WriteLine("=");
                //imprime el total en m3
                for (int i = 0; i < f11; i++)
                {
                    for (int j = 0; j < c22; j++)
                    {
                        Console.Write(m3[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("\t\tTu multiplicacion se a resuelto correctamente");
                Console.WriteLine("\n");
                Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
                Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
                Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            }
            Console.ReadLine();
        }

    }
}


