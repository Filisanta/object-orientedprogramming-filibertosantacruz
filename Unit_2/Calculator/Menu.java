using System;

namespace SalveM 
{
    class Program : Class1 
    {
        static void Main(string[] args)
        {
            Program unir = new Program();

            //Menu del usuario
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Cyan;


            Console.WriteLine("\t\t                                                   ");
            Console.WriteLine("\t\t                                                   ");
            Console.WriteLine("\t\t╔══════════════CREACION DE MATRICES═══════════════╗");
            Console.WriteLine("\t\t║               MATRIX CALCULATOR                 ║");
            Console.WriteLine("\t\t║           PRESS ENTER TO CONTINUE...            ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            int eleccion_menu = 0;
            do
            {
                Console.WriteLine("\t\t                                                    ");
                Console.WriteLine("\t\t                                                    ");
                Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
                Console.WriteLine("\t\t║   1. Basics                                     ║ ");
                Console.WriteLine("\t\t║   2. Gaussian                                   ║ ");
                Console.WriteLine("\t\t║   3. Gauss Jordan                               ║ ");
                Console.WriteLine("\t\t║   4. Inverse matrix                             ║ ");
                Console.WriteLine("\t\t║   5. Cramer                                     ║ ");
                Console.WriteLine("\t\t║   6. Sarrus                                     ║ ");
                Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                eleccion_menu  = int.Parse(Console.ReadLine());

                switch (eleccion_menu)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 1");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        Console.Clear();
                        Class1 basicas = new Class1();
                        basicas.Cargar();
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 2");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        Console.Clear();
                        Gaussian Gaus = new Gaussian ();
                        Gaus.Gauss();

                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 3");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        Console.Clear();
                        
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 4");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 5");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        Cramer Cram = new Cramer();
                        Cram.cramm();
                        Console.Clear();

                        break;
                        case 6:
                        Console.Clear();
                        Console.WriteLine("Has elegido la opcion 6");
                        Console.WriteLine("Presiona la tecla ENTER para continuar...");
                        Console.ReadLine();
                        sarrus sar = new sarrus();
                        sar.ala();
                        Console.Clear();

                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Has elegido una opcion Incorrecta");
                        break;
                }
            }
            while (eleccion_menu  != default);
            Console.ReadLine();
        }
    }
}
