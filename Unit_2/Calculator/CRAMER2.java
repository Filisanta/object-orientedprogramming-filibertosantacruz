using System;

namespace SalveM
{
    class Cramer
    {
        public void cramm()
        {
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║   La Matriz a resolver                          ║ ");
            Console.WriteLine("\t\t║   Ingresa el Valor X11                          ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
            Console.WriteLine("\t\t                                                    ");
            Console.WriteLine("\t\t                                                    ");
            

            Console.WriteLine("X11 Y11 Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double X11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y11");

            Console.WriteLine(X11 + " Y11 Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Y11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z11");

            Console.WriteLine(X11 + " " + Y11 + " Z11 T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Z11 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor T1");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " T1");
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double T1 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine("Ingresa el Valor X22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine("X22 Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double X22 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " Y22 Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Y22 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z22");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " Z22 T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double Z22 = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Ingresa el Valor T2");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " T2");
            Console.WriteLine("X33 Y33 Z33 T3");

            double T2 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine("Ingresa el Valor X33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine("X33 Y33 Z33 T3");

            double X33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Y33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " Y33 Z33 T3");

            double Y33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor Z33");

            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " Z33 T3");
            double Z33 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Ingresa el Valor T3");
            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " " + Z33 + " T3");
            double T3 = double.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Tu Matriz final es: ");
            Console.WriteLine(X11 + " " + Y11 + " " + Z11 + " " + T1);
            Console.WriteLine(X22 + " " + Y22 + " " + Z22 + " " + T2);
            Console.WriteLine(X33 + " " + Y33 + " " + Z33 + " " + T3);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            //Mostramos La matriz haciendo la primera operacion
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║        Primero obtenemos Delta                  ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");
            Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " " + Y11);
            Console.WriteLine();
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.WriteLine();
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine();
            //Esta es la operacion
            double Del = 0;
            Del = (X11 * Y22 * Z33);
            Del = Del + (Y11 * Z22 * X33);
            Del = Del + (Z11 * X22 * Y33);
            double ta = 0;
            ta = (Y11 * X22 * Z33);
            ta = ta + (X11 * Z22 * Y33);
            ta = ta + (Z11 * Y22 * X33);
            //se juntan las operaciones
            
            double Delta = Del - ta;
            Console.WriteLine("");
            //Aqui se muestra la operacion
            Console.Write("(" + X11 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + X33 + ")");
            Console.Write(" + "); Console.Write("(" + Z11 + " * " + X22 + " * " + Y33 + ")");
            //Mostramos La matriz haciendo la segunda operacion operacion
            Console.WriteLine("");
            Console.WriteLine("");
            Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " ");
            Console.Write(Y11);
            Console.WriteLine("");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22);
            Console.WriteLine("");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.WriteLine("");
            //esta es la segunda operacion
            
            Console.WriteLine("");
            //mostramos la segunda operacion
            Console.Write("(" + Z11 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + X11 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + X22 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("");
            //se muestra la operacion final
            Console.Write("(" + X11 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + Z22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Z11 + " * " + X22 + " * " + Y33 + ")");
            Console.Write(" - ");
            Console.Write("(" + Z11 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + "); Console.Write("(" + X11 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + "); Console.Write("(" + Y11 + " * " + X22 + " * " + Z33 + ")");
            Console.WriteLine("");
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Tu DELTA del sistema es:" + Delta);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");

            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗ ");
            Console.WriteLine("\t\t║              Calcular X                         ║ ");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝ ");

            //mostramos la matriz para sacar X
            //sera la primera operacion
            //mostramos la matriz para sacar X
             Console.Write(T1 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(T1 + " " + Y11);
            Console.Write("     ");
            Console.Write(T1 + " ");
            Console.Write(Y11 + " ");
            Console.Write(Z11 + " ");
            Console.Write(T1 + " ");
           Console.Write(Y11);
            Console.WriteLine();
             Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.Write("    ");
            Console.Write(T2 + " ");
            Console.Write(Y22 + " ");
            Console.Write(Z22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Y22);
            Console.WriteLine();
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.Write("    ");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            Console.Write(Z33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Y33 + " ");
            
            Console.WriteLine();
            //hacemos operaciones por separado
            double E = 0;
            E = (T1 * Y22 * Z33);
            E = E + (Y11 * Z22 * T3);
            E = E + (Z11 * T2 * Y33);
            //igual      
            double quis = 0;
            quis = (Y11 * T2 * Z33);
            quis = quis + (T1 * Z22 * Y33);
            quis = quis + (Z11 * Y22 * T3);
            //se juntan
            double Equis = E - quis;
            //se divide entre delta
            Equis = Equis / Delta;
            Console.WriteLine();
            //Aqui mostramos la operacion final
             Console.Write("(" + T1 + " * " + Y22 + " * " + Z33 + ")");
            Console.Write(" + ");  Console.Write("(" + Y11 + " * " + Z22 + " * " + T3 + ")");
            Console.Write(" + ");  Console.Write("(" + Z11 + " * " + T2 + " * " + Y33 + ")");
            Console.Write(" - ");  Console.Write("(" + Z11 + " * " + Y22 + " * " + T3 + ")");
            Console.Write(" + ");  Console.Write("(" + T1 + " * " + Z22 + " * " + Y33 + ")");
            Console.Write(" + ");  Console.Write("(" + Y11 + " * " + T2 + " * " + Z33 + ")");
            
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------------------");
            Console.WriteLine("                                            " + Delta);

            Console.WriteLine();
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Tu X es " + Equis);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
            Console.Clear();

            //mostramos la matriz para sacar Y       
            Console.WriteLine("Ahora sacaremos Y");
            Console.Write(X11 + " ");
            Console.Write(T1 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " " + T1);
            Console.Write("     ");
            Console.Write(X11 + " ");
            Console.Write(T1 + " ");
            Console.Write(Z11 + " ");
            Console.Write(X11 + " ");
            Console.Write(T1);
            Console.WriteLine();
            Console.Write(X22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(T2 + " ");
            Console.Write("    ");
            Console.Write(X22 + " ");
            Console.Write(T2 + " ");
            Console.Write(Z22 + " ");
            Console.Write(X22 + " ");
            Console.Write(T2);
            Console.WriteLine();
            Console.Write(X33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(T3 + " ");
            Console.Write("    ");
            Console.Write(X33 + " ");
            Console.Write(T3 + " ");
            Console.Write(Z33 + " ");
            Console.Write(X33 + " ");
            Console.Write(T3 + " ");
            
            Console.WriteLine();
            //se hace la operacion por separado
            double I = 0;
            I = (X11 * T2 * Z33);
            I = I + (T1 * Z22 * X33);
            I = I + (Z11 * X22 * T3);
            //igual
            double YYY = 0;
            YYY = (T1 * X22 * Z33);
            YYY = YYY + (X11 * Z22 * T3);
            YYY = YYY + (Z11 * T2 * X33);
            //se juntan
            double Yy = I - YYY;
            //SE DIVIDE ENTRE DELTA
            Yy = Yy / Delta;
            Console.WriteLine("");
            //Aqui mostramos la operacion final
             Console.Write("(" + X11 + " * " + T2 + " * " + Z33 + ")");
            Console.Write(" + ");
            Console.Write("(" + T1 + " * " + Z22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Z11 + " * " + X22 + " * " + T3 + ")");
            Console.Write(" - ");
            Console.Write("(" + Z11 + " * " + T2 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + X11 + " * " + Z22 + " * " + T3 + ")");
            Console.Write(" + ");
            Console.Write("(" + T1 + " * " + X22 + " * " + Z33 + ")");
            
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------------------");
            Console.WriteLine("                                            " + Delta);

            Console.WriteLine("");
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Tu Y es " + Yy);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
            Console.Clear();
            //mostramos la matriz para sacar Z
            Console.WriteLine("Ahora sacaremos Z");
             Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(T1 + " ");
            Console.Write(X11 + " " + Y11);
            Console.Write("     ");
            Console.Write(X11 + " ");
            Console.Write(Y11 + " ");
            Console.Write(T1 + " ");
            Console.Write(X11 + " ");
            Console.Write(Y11);
            Console.WriteLine();
             Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(T2 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write("    ");
            Console.Write(X22 + " ");
            Console.Write(Y22 + " ");
            Console.Write(T2 + " ");
            Console.Write(X22 + " ");
            Console.Write(Y22);
            Console.WriteLine();
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(T3 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write("    ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            Console.Write(T3 + " ");
            Console.Write(X33 + " ");
            Console.Write(Y33 + " ");
            
            Console.WriteLine();
            //se hacen las operaciones por separado
            double Ze = 0;
            Ze = (X11 * Y22 * T3);
            Ze = Ze + (Y11 * T2 * X33);
            Ze = Ze + (T1 * X22 * Y33);
            //igual
            double tA = 0;
            tA = (Y11 * X22 * T3);
            tA = tA + (X11 * T2 * Y33);
            tA = tA + (T1 * Y22 * X33);
            //se juntan
            double Zz = Ze - tA;
            //SE DIVIDE ENTRE DELTA
            Zz = Zz / Delta;
            Console.WriteLine("");
            //Aqui mostramos la operacion que se hizo
             Console.Write("(" + T1 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + X11 + " * " + T2 + " * " + Y33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + X22 + " * " + T3 + ")");
            Console.WriteLine("");
            Console.WriteLine("");
            //Aqui mostramos la operacion final
             Console.Write("(" + X11 + " * " + Y22 + " * " + T3 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + T2 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + T1 + " * " + X22 + " * " + Y33 + ")");
            Console.Write(" - ");
            Console.Write("(" + T1 + " * " + Y22 + " * " + X33 + ")");
            Console.Write(" + ");
            Console.Write("(" + X11 + " * " + T2 + " * " + Y33 + ")");
            Console.Write(" + ");
            Console.Write("(" + Y11 + " * " + X22 + " * " + T3 + ")");
            
            Console.WriteLine("");
            Console.WriteLine("--------------------------------------------------------------------------------------------");
            Console.WriteLine("                                            " + Delta);

            Console.WriteLine("");
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Tu Z es " + Zz);
            Console.WriteLine("\t\t╔═════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t║           Presiona ENTER para continuar         ║");
            Console.WriteLine("\t\t╚═════════════════════════════════════════════════╝");
            Console.ReadLine();
        }
    }
}
